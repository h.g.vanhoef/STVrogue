﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STVRogue.Utils;

namespace STVRogue.GameLogic
{

    

    public class Dungeon
    {
        public Node startNode;
        public Node exitNode;
        public uint difficultyLevel;
        /* a constant multiplier that determines the maximum number of monster-packs per node: */
        public uint M ;

        // list of all nodes
        public List<Node> Nodes = new List<Node>();
        
        /* To create a new dungeon with the specified difficult level and capacity multiplier */
        public Dungeon(uint level, uint nodeCapacityMultiplier)
        {
            Logger.log("Creating a dungeon of difficulty level " + level + ", node capacity multiplier " + nodeCapacityMultiplier + ".");
            difficultyLevel = level;
            if (level < 1)
            {
                throw new Exception("difficultylevel should be at least 1");
            }
            M = nodeCapacityMultiplier ;
            CreateDungeon(level);
        }

        void CreateDungeon(uint levels)
        {
            RandomGenerator rnd = new RandomGenerator();

            // level 1
            Tuple<Node,Node,Node,Node> level = CreateLevel(1);
            startNode = new Node("Start");
            startNode.connect(level.Item1);
            startNode.connect(level.Item2);

            for (uint i = 1; i <= levels; i++)
            {
                Bridge bridge = new Bridge("level " + (i - 1));
                Nodes.Add(bridge);
                // connect last node of level with bridge
                bridge.connectToNodeOfSameZone(level.Item3);
                bridge.connectToNodeOfSameZone(level.Item4);

                level = CreateLevel(i);

                bridge.connectToNodeOfNextZone(level.Item1);
                bridge.connectToNodeOfNextZone(level.Item2);
            }

            exitNode = new Node("Exit");
            exitNode.connect(level.Item3);
            exitNode.connect(level.Item4);
        }

        Tuple<Node,Node,Node,Node> CreateLevel(uint zone)
        {
            // list of all nodes between start and end nodes
            List<Node> nodes = new List<Node>();

            // to connect to previous bridge
            Node start1 = new Node();
            Node start2 = new Node();

            // to connect to next bridge
            Node end1 = new Node();
            Node end2 = new Node(); 

            // create 2 routes between the bridges
            int route1 = RandomGenerator.rnd.Next(2, 5);
            int route2 = RandomGenerator.rnd.Next(2, 5);

            Node currentNode = start1;
            for (int i = 0; i < route1; i++)
            {
                Node n = new Node();
                currentNode.connect(n);
                currentNode = n;
                nodes.Add(n);
            }
            currentNode.connect(end1);
            currentNode = start2;
            for (int i = 0; i < route2; i++)
            {
                Node n = new Node();
                currentNode.connect(n);
                currentNode = n;
                nodes.Add(n);
            }
            currentNode.connect(end2);

            // calculate current total of connections
            int connections = 12 + 2 * route1 + 2 * route2;
            int nodecount = 6 + route1 + route2;

            // create randomly connected nodes
            int no = RandomGenerator.rnd.Next(5, 20);
            for (int i = 0; i < no; i++)
            {
                Node n = new Node();

                int tries = 0;

                while(n.neighbors.Count == 0 || (n.neighbors.Count < 4 && connections / nodecount < 3 && tries < 5))
                {
                    Node neighbour = nodes[RandomGenerator.rnd.Next(0, nodes.Count - 1)];
                    if (neighbour.neighbors.Count < 4 && !neighbour.neighbors.Contains(n) && neighbour.GetType() != typeof(Bridge))
                    {
                        n.connect(neighbour);
                        connections += 2;
                    }
                    tries++;
                }

                nodecount++;
                nodes.Add(n);
            }

            // wrapping up
            Nodes.AddRange(nodes);
            Nodes.Add(start1);
            Nodes.Add(start2);
            Nodes.Add(end1);
            Nodes.Add(end2);

            foreach (Node n in nodes)
            {
                n.zone = zone;
            }
            start1.zone = zone;
            start2.zone = zone;
            end1.zone = zone;
            end2.zone = zone;

            return new Tuple<Node, Node, Node, Node>(start1, start2, end1, end2);
        }
        
        /* Return a shortest path between node u and node v */
        public List<Node> shortestpath(Node u, Node v)
        {
            // do a BFS over nodes
            Queue<Node> queue = new Queue<Node>();
            queue.Enqueue(v);
            Node found = null;

            ResetBFS();

            while (queue.Count > 0 && found == null)
            {
                Node d = queue.Dequeue();
                foreach (Node n in d.neighbors)
                {
                    if (n == u) found = n;

                    if (!n.done)
                    {
                        n.done = true;
                        queue.Enqueue(n);
                        n.prev = d;
                    }
                }
            }

            // backtrack route
            if (found != null)
            {
                List<Node> answer = new List<Node>();
                do
                {
                    answer.Add(found);
                    found = found.prev;
                }
                while (found != v);

                return answer;
            }

            // no route between n and v is found
            throw new Exception("No route found");
        }

        // set done to false and prev to null
        void ResetBFS()
        {
            for (int i = 0; i < Nodes.Count; i++)
            {
                Nodes[i].done = false;
                Nodes[i].prev = null;
            }
        }
        
        
        /* To disconnect a bridge from the rest of the zone the bridge is in. */
        public void disconnect(Bridge b) {
            Logger.log("Disconnecting the bridge " + b.id + " from its zone.");
            List<Node> fromNodes = b.getFromNodes();
            for (int i = 0; i < fromNodes.Count; i++)
            {
                b.neighbors.Remove(fromNodes[i]);
                fromNodes[i].neighbors.Remove(b);
            }
            b.setFromNodes(new List<Node>());
        }

        /* To calculate the level of the given node. */
        public uint level(Node d)
        {
            return d.level;
        }

        public uint nodeCapacity(Node d)
        {
            return M * (level(d) + 1);
        }
    } 

    public class Node
    {
        public string id;
        public List<Node> neighbors = new List<Node>();
        public List<Pack> packs = new List<Pack>();
        public List<Item> items = new List<Item>();

        // needed by BFS
        public Node prev;
        public bool done;

        public uint level;
        public uint zone;

        public Node() { }
        public Node(String id) { this.id = id; }
        
        /* To connect this node to another node. */
        public void connect(Node nd)
        {
            if (neighbors.Count >= 4 || nd.neighbors.Count >= 4)
                throw new GameCreationException();
            if (neighbors.Contains(nd) || nd.neighbors.Contains(this))
                throw new GameCreationException();
            if (nd == this)
                throw new GameCreationException();
            neighbors.Add(nd); nd.neighbors.Add(this);
        }
        
        /* To disconnect this node from the given node. */
        public void disconnect(Node nd)
        {
            neighbors.Remove(nd); nd.neighbors.Remove(this);
        }


        /* Execute a fight between the player and the packs in this node.
         * Such a fight can take multiple rounds as describe in the Project Document.
         * A fight terminates when either the node has no more monster-pack, or when
         * the player's HP is reduced to 0. 
         */
        public void fight(Player player)
        {
            bool turn = true;
            while (player.HP > 0 && packs.Count > 0)
            {
                if (turn)
                {
                    player.Attack(packs[0].members[0]);
                    turn = false;
                    // remove empty pack
                    if (packs[0].members.Count == 0)
                    {
                        packs.RemoveAt(0);
                    }
                }
                else
                {
                    int packHP = 0;
                    for (int i = 0; i < packs[0].members.Count; i++)
                    {
                        packHP += packs[0].members[i].HP;
                    }
                    int fleeChance = 100 * (1 - packHP / packs[0].startingHP) / 2;
                    int flee = RandomGenerator.rnd.Next(0, 100);
                    bool flown = false;
                    if (flee <= fleeChance)
                    {
                        flown = fleeNode(packs[0]); 
                    }
                    if (!flown)
                    {
                        packs[0].Attack(player);
                    }
                    else if (packs.Count > 1)
                    {
                        packs[1].Attack(player);
                    }
                }
            }
        }

        public bool fleeNode(Pack pack)
        {
            bool succeeded = false;
            int i = 0;
            while (i < neighbors.Count && !succeeded)
            {
                succeeded = packs[0].move(neighbors[i]);
                i++;
            }
            return succeeded;
        }
    }

    public class Bridge : Node
    {
        List<Node> fromNodes = new List<Node>();
        List<Node> toNodes = new List<Node>();
        public Bridge(String id) : base(id) {  }
        
        // getters and setters
        public void setFromNodes(List<Node> fromNodes)
        {
            this.fromNodes = fromNodes;
        }

        public List<Node> getFromNodes()
        {
            return fromNodes;
        }
        /* Use this to connect the bridge to a node from the same zone. */
        public void connectToNodeOfSameZone(Node nd) 
        {
            base.connect(nd);
            fromNodes.Add(nd);
        }

        /* Use this to connect the bridge to a node from the next zone. */
        public void connectToNodeOfNextZone(Node nd)
        {
            base.connect(nd);
            toNodes.Add(nd);
        }

    }

}
