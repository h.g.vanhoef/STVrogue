﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STVRogue.Utils;

namespace STVRogue.GameLogic
{

    

    public class Dungeon
    {
        public Node startNode;
        public Node exitNode;
        public uint difficultyLevel;
        /* a constant multiplier that determines the maximum number of monster-packs per node: */
        public uint M ; 
        
        /* To create a new dungeon with the specified difficult level and capacity multiplier */
        public Dungeon(uint level, uint nodeCapacityMultiplier)
        {
            Logger.log("Creating a dungeon of difficulty level " + level + ", node capacity multiplier " + nodeCapacityMultiplier + ".");
            difficultyLevel = level;
            M = nodeCapacityMultiplier ;
            throw new NotImplementedException();
        }

        void CreateDungeon(uint levels)
        {
            RandomGenerator rnd = new RandomGenerator();

            Node[] level = CreateLevel();
            startNode = level[0];

            int i = 1;
            do
            {
                Bridge currentBridge = new Bridge("bridge " + i);

                int c = RandomGenerator.rnd.Next(1, level.Count());
                currentBridge.connectToNodeOfSameZone(level[c]);

                level = CreateLevel();

                currentBridge.connectToNodeOfNextZone(level[0]);

                c = RandomGenerator.rnd.Next(1, level.Count());
                currentBridge.connectToNodeOfNextZone(level[c]);

                i++;
            }
            while (i <= levels);
        }

        // create a level of 5 - 10 nodes
        Node[] CreateLevel()
        {
            int nodes = RandomGenerator.rnd.Next(5, 10);

            Node[] level = new Node[nodes];

            int totalconnections = 0;

            for (int i = 0; i < nodes; i++)
            {
                level[i] = new Node();

                int connections = 0;
                int j = i;
                while (j-- >= 0 && connections < 4 && totalconnections < i * 3)
                {
                    if (j == i - 1 || RandomGenerator.rnd.Next(0,1) == 1)
                    {
                        level[i].connect(level[j]);
                        connections++;
                        totalconnections++;
                    }
                }
            }

            return level;
        }
        
        /* Return a shortest path between node u and node v */
        public List<Node> shortestpath(Node u, Node v)
        {
            // do a BFS over nodes
            Queue<Node> queue = new Queue<Node>();
            queue.Enqueue(v);
            Node found = null;

            ResetBFS();

            while (queue.Count > 0 && found == null)
            {
                Node d = queue.Dequeue();
                foreach (Node n in d.neighbors)
                {
                    if (n == u) found = n;

                    if (!n.done)
                    {
                        n.done = true;
                        queue.Enqueue(n);
                        n.prev = d;
                    }
                }
            }

            // backtrack route
            if (found != null)
            {
                List<Node> answer = new List<Node>();
                do
                {
                    answer.Add(found);
                    found = found.prev;
                }
                while (found != v);

                return answer;
            }

            // no route between n and v is found
            throw new Exception("No route found");
        }

        // set done to false and prev to null
        void ResetBFS()
        {
            throw new NotImplementedException();
        }
        
        
        /* To disconnect a bridge from the rest of the zone the bridge is in. */
        public void disconnect(Bridge b) {
            Logger.log("Disconnecting the bridge " + b.id + " from its zone.");
            throw new NotImplementedException();
        }

        /* To calculate the level of the given node. */
        public uint level(Node d) { throw new NotImplementedException(); }
    } 

    public class Node
    {
        public String id;
        public List<Node> neighbors = new List<Node>();
        public List<Pack> packs = new List<Pack>();
        public List<Item> items = new List<Item>();

        // needed by BFS
        public Node prev;
        public bool done;

        uint level;

        public Node() { }
        public Node(String id) { this.id = id; }
        
        /* To connect this node to another node. */
        public void connect(Node nd)
        {
            neighbors.Add(nd); nd.neighbors.Add(this);
        }
        
        /* To disconnect this node from the given node. */
        public void disconnect(Node nd)
        {
            neighbors.Remove(nd); nd.neighbors.Remove(this);
        }


        /* Execute a fight between the player and the packs in this node.
         * Such a fight can take multiple rounds as describe in the Project Document.
         * A fight terminates when either the node has no more monster-pack, or when
         * the player's HP is reduced to 0. 
         */
        public void fight(Player player)
        {
            throw new NotImplementedException();
        }
    }

    public class Bridge : Node
    {
        List<Node> fromNodes = new List<Node>();
        List<Node> toNodes = new List<Node>();
        public Bridge(String id) : base(id) {  }
        
        /* Use this to connect the bridge to a node from the same zone. */
        public void connectToNodeOfSameZone(Node nd) 
        {
            base.connect(nd);
            fromNodes.Add(nd);
        }

        /* Use this to connect the bridge to a node from the next zone. */
        public void connectToNodeOfNextZone(Node nd)
        {
            base.connect(nd);
            toNodes.Add(nd);
        }
    }

}
