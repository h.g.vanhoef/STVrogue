﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using STVRogue.Utils;
using static STVRogue.Utils.MSTest_Predicates;

namespace STVRogue.GameLogic
{
    /* An example of a test class written using VisualStudio's own testing
     * framework. 
     * This one is to unit-test the class Player. The test is incomplete though, 
     * as it only contains two test cases. 
     */
    [TestClass]
    public class MSTest_Player
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void MSTest_use_onEmptyBag()
        {
            Player P = new Player();
            P.use(new Item());
        }

        [TestMethod]
        public void MSTest_use_item_in_bag()
        {
            Player P = new Player();
            Item x = new HealingPotion("pot1");
            P.bag.Add(x);
            P.use(x);
            Assert.IsFalse(P.bag.Contains(x));
        }

        [TestMethod]
        public void MSTest_Attack()
        {
            Player P = new Player();
            Pack pack = new Pack("trolls", 1);
            Monster foe = pack.members[0];
            int expectedHP = (int)Math.Max(foe.HP - P.AttackRating, 0);
            P.Attack(foe);
            int actualHP = foe.HP;
            Assert.AreEqual(expectedHP, actualHP);
        }

        [TestMethod]
        public void killingAMonster()
        {
            Player P = new Player();
            Pack pack = new Pack("trolls", 1);
            Monster foe = pack.members[0];
            foe.HP = 1;
            int kills = (int)P.KillPoint;
            P.Attack(foe);
            Assert.AreEqual(kills + 1, (int)P.KillPoint);
        }

        [TestMethod]
        public void MSTest_Attack_accelerated()
        {
            Player P = new Player();
            P.accelerated = true;
            uint packmembers = (uint)Utils.RandomGenerator.rnd.Next(2, 10);
            Pack pack = new Pack("trolls", packmembers);

            int[] expectedHPs = new int[packmembers];
            Monster[] monsters = new Monster[packmembers];
            for (int i = 0; i < packmembers; i++)
            {
                Monster foe = pack.members[i];
                expectedHPs[i] = (int)Math.Max(foe.HP - P.AttackRating, 0);
                monsters[i] = foe;
            }

            P.Attack(pack.members[0]);

            for (int i = 0; i < packmembers; i++)
            {
                int actualHP = monsters[i].HP;
                Assert.AreEqual(expectedHPs[i], actualHP);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void MSTest_Attack_notAMonster()
        {
            Player P = new Player();
            Player B = new Player();
            P.Attack(B);
        }
    }

    [TestClass]
    public class MSTest_GameCreationException
    {
        [TestMethod]
        public void gameCreationException()
        {
            GameCreationException g = new GameCreationException();
            Assert.IsInstanceOfType(g, typeof(GameCreationException));
        }

        [TestMethod]
        public void gameCreationExceptionText()
        {
            GameCreationException g = new GameCreationException("game creation failed!");
            Assert.IsInstanceOfType(g, typeof(GameCreationException));
        }
    }

    [TestClass]
    public class MSTest_Items
    {
        [TestMethod]
        public void alreadyUsedItem()
        {
            Player P = new Player();
            HealingPotion x = new HealingPotion("id");
            P.bag.Add(x);
            x.use(P);
            P.bag.Add(x);
            x.use(P);
            //TODO: what to test?
        }
    }

    [TestClass]
    public class MSTest_Crystal
    {
        [TestMethod]
        public void useCrystal()
        {
            Player P = new Player();
            P.dungeon = new Dungeon(1, 2);
            P.location = new Bridge("id");
            Crystal x = new Crystal("id");
            P.bag.Add(x);
            P.use(x);
            Assert.IsTrue(P.accelerated);
            int fromNodes = (P.location as Bridge).getFromNodes().Count;
            Assert.AreEqual(0, fromNodes);
        }
    }

    [TestClass]
    public class MSTest_Dungeon
    {
        Dungeon dungeon = new Dungeon(10, 0);
        Predicates predicates = new Predicates();

        // test if dungeon is fully connected
        [TestMethod]
        public void FullyConnected()
        {
            foreach (Node n in dungeon.Nodes)
            {
                Assert.IsTrue(predicates.isReachable(dungeon.startNode, n));
            }
        }

        // test if dungeon is a valid dungeon
        [TestMethod]
        public void ValidDungeon()
        {
            Assert.IsTrue(predicates.isValidDungeon(dungeon.startNode, dungeon.exitNode, 10));
        }

        [TestMethod]
        public void ValidBridges()
        {
            foreach (Node n in dungeon.Nodes)
            {
                if (n.GetType() == typeof(Bridge))
                {
                    Assert.IsTrue(predicates.isBridge(dungeon.startNode, dungeon.exitNode, n));
                }
            }
        }

        [TestMethod]
        public void NumberOfBridges()
        {
            Assert.IsTrue(predicates.countNumberOfBridges(dungeon.startNode, dungeon.exitNode) == 10);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TooLowLevel()
        {
            new Dungeon(0, 100);
        }

        [TestMethod]
        public void Disconnect()
        {
            Bridge bridge = null;
            foreach (Node n in dungeon.Nodes)
            {
                if (n.GetType() == typeof(Bridge))
                {
                    bridge = (Bridge)n;
                    dungeon.disconnect(bridge);
                    Assert.AreEqual(bridge.neighbors.Count, 2);
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void noPathFound()
        {
            Dungeon d = new Dungeon(2, 5);
            Node dirtyNode = new Node("id");
            d.Nodes.Add(dirtyNode);
            d.shortestpath(d.startNode, dirtyNode);
        }

        [TestMethod]
        public void whatNodeCapacity()
        {
            Node node = new Node("id");
            node.level = 0;
            Dungeon d = new Dungeon(2, 5);
            uint expectedNodeCapacity = 5;
            Assert.AreEqual(expectedNodeCapacity, d.nodeCapacity(node));
        }
    }

    [TestClass]
    public class MSTest_Game
    {
        [TestMethod]
        public void createAndUpdateGame()
        {
            Game g = new Game(5, 2, 20);
            Assert.IsNotNull(g.dungeon);
            Assert.IsNotNull(g.player);
            Command c = new Command();
            bool update = g.update(c);
            Assert.IsTrue(update);
        }
    }

    [TestClass]
    public class MSTest_Pack
    {
        [TestMethod]
        public void attackPlayer()
        {
            Pack pack = new Pack("id", 3);
            Player P = new Player();
            int expectedHP = P.HP - 3;
            pack.Attack(P);
            Assert.AreEqual(expectedHP, P.HP);
        }

        [TestMethod]
        public void killPlayer()
        {
            Pack pack = new Pack("id", 100);
            Player P = new Player();
            int expectedHP = 0;
            pack.Attack(P);
            Assert.AreEqual(P.HP, expectedHP);
        }

        [TestMethod]
        public void moveTowards()
        {
            Pack pack = new Pack("id", 1);
            Dungeon d = new Dungeon(5, 2);
            pack.location = d.startNode;
            pack.dungeon = d;
            List<Node> list = d.shortestpath(d.startNode, d.exitNode);
            Node expectedLocation = list[1];
            pack.moveTowards(d.exitNode);
            Assert.AreEqual(pack.location, expectedLocation);
        }

        [TestMethod]
        public void move()
        {
            Pack pack = new Pack("id", 1);
            Dungeon d = new Dungeon(5, 2);
            Node expectedLocation = d.Nodes[0].neighbors[0];
            pack.location = d.Nodes[0];
            pack.dungeon = d;
            pack.move(d.Nodes[0].neighbors[0]);
            Assert.AreEqual(pack.location, expectedLocation);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void moveToNonNeigborNode()
        {
            Pack pack = new Pack("id", 1);
            Dungeon d = new Dungeon(5, 2);
            pack.location = d.startNode;
            pack.move(d.exitNode);
        }

        [TestMethod]
        public void moveToFullPacksNode()
        {
            Pack pack1 = new Pack("1", 1);
            Pack pack2 = new Pack("2", 1);
            Dungeon d = new Dungeon(5, 1);
            pack1.location = d.Nodes[0];
            pack1.dungeon = d;
            pack2.location = d.Nodes[0].neighbors[0];
            d.Nodes[0].neighbors[0].packs.Add(pack2);
            bool b = pack1.move(d.Nodes[0].neighbors[0]);
            Assert.IsFalse(b);
        }

        [TestMethod]
        public void moveToFullMonstersNode()
        {
            Pack pack = new Pack("id", 2);
            Dungeon d = new Dungeon(5, 1);
            Node expectedLocation = d.Nodes[0].neighbors[0];
            pack.location = d.Nodes[0];
            pack.dungeon = d;
            bool b = pack.move(d.Nodes[0].neighbors[0]);
            Assert.IsFalse(b);
        }

        [TestMethod]
        public void getStartingHP()
        {

        }
    }

    [TestClass]
    public class MSTest_Node
    {
        [TestMethod]
        public void nodeConnectAndDisconnect()
        {
            Node node1 = new Node("1");
            Node node2 = new Node("2");
            node1.connect(node2);
            Assert.AreEqual(node1.neighbors[0], node2);
            Assert.AreEqual(node2.neighbors[0], node1);
            node1.disconnect(node2);
            Assert.AreEqual(0, node1.neighbors.Count);
            Assert.AreEqual(0, node2.neighbors.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(GameCreationException))]
        public void nodeConnectOneToMany()
        {
            Node node1 = new Node("1");
            Node node2 = new Node("2");
            Node node3 = new Node("3");
            Node node4 = new Node("4");
            Node node5 = new Node("5");
            Node node6 = new Node("6");
            node1.connect(node2);
            node1.connect(node3);
            node1.connect(node4);
            node1.connect(node5);
            node1.connect(node6);
        }

        [TestMethod]
        [ExpectedException(typeof(GameCreationException))]
        public void nodeConnectItself()
        {
            Node node1 = new Node("1");
            node1.connect(node1);
        }

        [TestMethod]
        [ExpectedException(typeof(GameCreationException))]
        public void nodeConnectSameNodeTwice()
        {
            Node node1 = new Node("1");
            Node node2 = new Node("2");
            node1.connect(node2);
            node1.connect(node2);
        }

        [TestMethod]
        public void nodePackFlees()
        {
            Node node1 = new Node("1");
            Node node2 = new Node("2");
            node1.connect(node2);
            node1.level = 1;
            node2.level = 1;
            Dungeon d = new Dungeon(1, 1);
            d.Nodes.Add(node1);
            d.Nodes.Add(node2);
            Pack pack = new Pack("id", 1);
            pack.location = node1;
            pack.dungeon = d;
            node1.packs.Add(pack);
            node1.fleeNode(pack);
            Assert.AreEqual(0, node1.packs.Count);
            Assert.AreEqual(1, node2.packs.Count);
        }

        [TestMethod]
        public void Fight()
        {
            // fight should take place in a dungeon
            Dungeon D = new Dungeon(10, 10);

            for (int i = 0; i < 1000; i++)
            {
                Node node = D.Nodes[D.Nodes.Count - 2];
                for (int t = 0; t < 5; t++)
                {
                    Pack pack = new Pack("foe", (uint)RandomGenerator.rnd.Next(1, 2));
                    pack.location = node;
                    pack.dungeon = D;
                    node.packs.Add(pack);
                }
                Player p = new Player();
                node.fight(p);
                Assert.IsTrue(PackHP(node) == 0 || p.HP == 0);
            }
        }
    }
}
